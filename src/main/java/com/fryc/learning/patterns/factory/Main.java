package com.fryc.learning.patterns.factory;



public class Main{
    public static void main(String[] args) {
        CarFactory carFactory = new CarFactory();
        Car sportCar = carFactory.getCar(CarFactory.CarType.SPORT);
        sportCar.drive();
    }
}
