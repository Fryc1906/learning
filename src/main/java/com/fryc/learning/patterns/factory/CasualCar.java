package com.fryc.learning.patterns.factory;

public class CasualCar implements Car {
    public void drive() {
        System.out.println("You are driving casual car");
    }
}
