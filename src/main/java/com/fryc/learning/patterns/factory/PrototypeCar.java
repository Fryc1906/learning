package com.fryc.learning.patterns.factory;

public class PrototypeCar implements Car {
    public void drive() {
        System.out.println("You are driving prototype car");
    }
}
