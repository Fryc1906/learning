package com.fryc.learning.patterns.factory;

public class CarFactory {

    public enum CarType{
        SPORT,
        CASUAL,
        PROTOTYPE
    }

    public Car getCar(CarType type){
        switch(type){
            case SPORT:{
                return new SportCar();
            }
            case CASUAL:{
                return new CasualCar();
            }
            case PROTOTYPE:{
                return new PrototypeCar();
            }
            default:{
                return null;
            }
        }
    }
}
