package com.fryc.learning.patterns.factory;

public interface Car {
    void drive();
}
