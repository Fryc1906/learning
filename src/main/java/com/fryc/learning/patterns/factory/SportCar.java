package com.fryc.learning.patterns.factory;

public class SportCar implements Car {
    public void drive() {
        System.out.println("You are driving sport car");
    }
    public void goOver200mph(){
        System.out.println("Yeah! Feel the power!");
    }
}
