package com.fryc.learning.patterns.singletone;

public class Main {
    public static void main(String[] args) {
        MySingletoneClass singletone = MySingletoneClass.getInstance();
        MySingletoneClass singletone2 = MySingletoneClass.getInstance();
        if(singletone == singletone2 && singletone.equals(singletone)){
            System.out.println(singletone.toString()+"\n"+singletone2.toString()+"\nThe same instance");
        }
    }
}
