package com.fryc.learning.patterns.singletone;

public class MySingletoneClass {
    private static MySingletoneClass instance;

    private MySingletoneClass(){
    }

    static MySingletoneClass getInstance(){
        if(instance == null){
            instance = new MySingletoneClass();
        }
        return instance;
    }

}
