package com.fryc.learning.patterns.abstract_factory;

public class HumanDriver implements Driver {
    public void getDriverName() {
        System.out.println("Rick");
    }
}
