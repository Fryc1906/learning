package com.fryc.learning.patterns.abstract_factory;

import com.fryc.learning.patterns.factory.Car;
import com.fryc.learning.patterns.factory.CasualCar;
import com.fryc.learning.patterns.factory.PrototypeCar;
import com.fryc.learning.patterns.factory.SportCar;

public class CarFactory extends AbstractFactory {

    Driver getDriver(DriverFactory.DriverType type) {
        return null;
    }

    public enum CarType{
        SPORT,
        CASUAL,
        PROTOTYPE
    }

    Car getCar(CarType type){
        switch(type){
            case SPORT:{
                return new SportCar();
            }
            case CASUAL:{
                return new CasualCar();
            }
            case PROTOTYPE:{
                return new PrototypeCar();
            }
            default:{
                return null;
            }
        }
    }
}
