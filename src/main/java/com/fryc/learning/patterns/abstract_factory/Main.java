package com.fryc.learning.patterns.abstract_factory;

import com.fryc.learning.patterns.factory.*;

public class Main{
    public static void main(String[] args) {
        AbstractFactory carFactory = FactoryProducer.getFactory(AbstractFactory.FactoryType.CAR);
        Car casualCar = carFactory.getCar(CarFactory.CarType.CASUAL);
        Car prototypeCar = carFactory.getCar(CarFactory.CarType.PROTOTYPE);

        AbstractFactory driverFactory = FactoryProducer.getFactory(AbstractFactory.FactoryType.DRIVER);
        Driver humanDriver = driverFactory.getDriver(DriverFactory.DriverType.HUMAN);
        Driver artificialDriver = driverFactory.getDriver(DriverFactory.DriverType.ARTIFICIAL);

        casualCar.drive();
        prototypeCar.drive();

        humanDriver.getDriverName();
        artificialDriver.getDriverName();
    }
}
