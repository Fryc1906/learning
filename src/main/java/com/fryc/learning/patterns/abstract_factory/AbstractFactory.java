package com.fryc.learning.patterns.abstract_factory;

import com.fryc.learning.patterns.factory.Car;

abstract class AbstractFactory {

    public enum FactoryType{
        CAR,
        DRIVER,
    }

    abstract Car getCar(CarFactory.CarType type);
    abstract Driver getDriver(DriverFactory.DriverType type);
}
