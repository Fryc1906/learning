package com.fryc.learning.patterns.abstract_factory;

public class FactoryProducer {
    public static AbstractFactory getFactory(AbstractFactory.FactoryType type){
        switch(type){
            case CAR:{
                return new CarFactory();
            }
            case DRIVER:{
                return new DriverFactory();
            }
            default:
                return null;
        }
    }
}
