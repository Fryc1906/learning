package com.fryc.learning.patterns.abstract_factory;

public interface Driver {
    void getDriverName();
}
