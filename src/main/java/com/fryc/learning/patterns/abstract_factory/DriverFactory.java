package com.fryc.learning.patterns.abstract_factory;

import com.fryc.learning.patterns.factory.Car;

public class DriverFactory extends AbstractFactory {
    public enum DriverType{
        HUMAN,
        ARTIFICIAL
    }

    Car getCar(com.fryc.learning.patterns.abstract_factory.CarFactory.CarType type) {
        return null;
    }

    public Driver getDriver(DriverFactory.DriverType type){
        switch(type){
            case HUMAN:{
                return new HumanDriver();
            }
            case ARTIFICIAL:{
                return new ArtificialDriver();
            }
            default:{
                return null;
            }
        }
    }
}
