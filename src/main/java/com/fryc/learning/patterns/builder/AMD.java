package com.fryc.learning.patterns.builder;

public class AMD extends Processor {
    public String name() {
        return "Ryzen 7";
    }

    public String manufacturer() {
        return "AMD";
    }
}
