package com.fryc.learning.patterns.builder;

public abstract class GraphicCard implements Part{
    public Cooling cooling() {
        return new HeatPipe();
    }

    public double price() {
        return 1500.00D;
    }
}
