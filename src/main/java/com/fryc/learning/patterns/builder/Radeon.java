package com.fryc.learning.patterns.builder;

public class Radeon extends GraphicCard {

    public String name() {
        return "Radeon HD4850";
    }

    public String manufacturer() {
        return "Asus";
    }
}
