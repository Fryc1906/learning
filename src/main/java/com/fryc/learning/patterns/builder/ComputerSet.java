package com.fryc.learning.patterns.builder;

import java.util.ArrayList;
import java.util.List;

public class ComputerSet {
    private List<Part> parts = new ArrayList<Part>();

    public void addPart(Part part){
        parts.add(part);
    }

    public double getCost(){
        double cost = 0.00D;

        for(Part part : parts){
            cost += part.price();
        }
        return cost;
    }

    public void showParts(){
        for(Part part : parts){
            System.out.println(part.cooling().withFan());
            System.out.println(part.manufacturer());
            System.out.println(part.name());
            System.out.println(part.price());
        }
    }

}




