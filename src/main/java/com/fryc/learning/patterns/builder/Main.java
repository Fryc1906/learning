package com.fryc.learning.patterns.builder;

public class Main {
    public static void main(String[] args) {
        CompSetBuilder builder = new CompSetBuilder();

        ComputerSet set1 = builder.makeSet1();
        ComputerSet set2 = builder.makeSet2();

        set1.showParts();
        set2.showParts();

        System.out.println(set1.getCost());
        System.out.println(set2.getCost());

    }
}
