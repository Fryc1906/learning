package com.fryc.learning.patterns.builder;

public class HeatPipe implements Cooling {

    public boolean withFan() {
        return false;
    }
}
