package com.fryc.learning.patterns.builder;

public interface Cooling {
    boolean withFan();
}
