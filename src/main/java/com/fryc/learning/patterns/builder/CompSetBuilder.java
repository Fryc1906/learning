package com.fryc.learning.patterns.builder;

public class CompSetBuilder {

    public ComputerSet makeSet1() {
        ComputerSet set = new ComputerSet();
        set.addPart(new AMD());
        set.addPart(new Radeon());
        return set;
    }

    public ComputerSet makeSet2(){
        ComputerSet set = new ComputerSet();
        set.addPart(new Intel());
        set.addPart(new GForce());
        set.addPart(new Radeon());
        return set;
    }
}
