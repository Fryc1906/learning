package com.fryc.learning.patterns.builder;

abstract class Processor implements Part {
    public Cooling cooling() {
        return new Fan();
    }

    public double price() {
        return 1000.00D;
    }
}
