package com.fryc.learning.patterns.builder;

public class GForce extends GraphicCard {
    public String name() {
        return "GTX 1050Ti";
    }

    public String manufacturer() {
        return "Gigabyte";
    }
}
