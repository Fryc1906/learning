package com.fryc.learning.patterns.builder;

public class Intel extends Processor {
    public String name() {
        return "I7";
    }

    public String manufacturer() {
        return "Intel";
    }
}
