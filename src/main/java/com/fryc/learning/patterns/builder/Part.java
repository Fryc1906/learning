package com.fryc.learning.patterns.builder;

public interface Part {
    String name();
    Cooling cooling();
    String manufacturer();
    double price();
}
