package com.fryc.learning.patterns.builder;

public class Fan implements Cooling {
    public boolean withFan() {
        return true;
    }
}
